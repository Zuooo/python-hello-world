#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
Centos7 Python3.7
Note
'''

import urllib.request,re,time,random,gzip

def saveFile(str,filename):
    path = "./"+filename
    file = open(path,'wb')
    for d in data:
        d = str(d)+'\n'
        file.write(d.encode('utf-8'))
    file.close()

def ungzip(data):
    try:
        #print("正在解压缩...")
        data = gzip.decompress(data)
        #print("解压完毕...")
    except:
        print("未经压缩，无需解压...")
    return data

class Spider:
    def __init__(self,pageIdx=1,url="https://blog.csdn.net/dqcfkyqdxym3f8rb0/article/category/7353144/1"):
        #默认当前页
        self.pageIdx = pageIdx
        self.url = url[0:url.rfind('/') + 1] + str(pageIdx)
        self.headers = {
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0;WOW64) AppleWebKit/537.36 " "(KHTML,like Gecko) Chrome/51.0.2704.63 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, sdch",
            "Accept-Language": "zh-CN,zh;q=0.8",
        }


    def getData(self):
        ret=[]
        str = r'<div.*?class="article-item-box.*?csdn-tracking-statistics"'+\
              r'.*?data-articleid="(.+?)">.*?<h4.*?/span>(.*?)</a>.*?</h4>.*?<a.*?>(.*?)</a>'+\
              r'.*?<span.*?date.*?>(.*?)</span.*?</div>'
        req = urllib.request.Request(url=self.url, headers=self.headers)
        res = urllib.request.urlopen(req)
        # 从我的csdn博客主页抓取的内容是压缩后的内容，先解压缩
        data = res.read()
        data = ungzip(data)
        data = data.decode('utf-8')
        #print(data)
        #pattern = re.compile(str,re.DOTALL)
        pattern = re.compile(str,re.DOTALL)
        items = re.findall(pattern,data)
        #print(items)
        #exit()
        for item in items:
            d = {
                'id':item[0].strip(),
                'title':item[1].strip(),
                'desc':item[2].strip(),
                'time': item[3].strip()
            }
            ret.append(d)
        return ret

    def getDetailData(self,url):
        url = url if url else self.url
        str = r'<h1\sclass="title-article">(.*?)</h1>' +\
              r'.*?<span\sclass="time">(.*?)</span>'
        req = urllib.request.Request(url=url, headers=self.headers)
        res = urllib.request.urlopen(req)
        data = res.read()
        data = ungzip(data)
        data = data.decode('utf-8')
        pattern = re.compile(str,re.DOTALL)
        item = re.findall(pattern, data)
        ret = {
            'title': item[0].strip()
        }
        return ret

s = Spider()

#papers = s.getData()
#print(papers)

detail = s.getDetailData('https://blog.csdn.net/dQCFKyQDXYm3F8rB0/article/details/83016359')
print(detail)

